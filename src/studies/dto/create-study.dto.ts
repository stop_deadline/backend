import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsEnum, IsOptional } from 'class-validator';
import { CheckType } from './studies.enums';

export class CreateStudyDto {
  @ApiProperty({
    example: 'EXAM',
    description: 'type',
    enum: CheckType,
  })
  @IsNotEmpty()
  @IsString()
  @IsEnum(CheckType)
  type: CheckType;

  @ApiProperty({
    example: 'Проектная деятельность',
    description: 'name',
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    example: 'преподаватель',
    description: 'teacher',
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  teacher: string;

  @ApiProperty({
    example: 'https://localhost:3000/...',
    description: 'link',
    type: String,
  })
  @IsOptional()
  @IsString()
  link?: string;

  @ApiProperty({
    example: 'example@gmail.com',
    description: 'link',
    type: String,
  })
  @IsOptional()
  @IsString()
  connection?: string;
}
