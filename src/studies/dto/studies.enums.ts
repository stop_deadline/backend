export enum CheckType {
  EXAM,
  CREDIT,
  DIFFERENTIATED_CREDIT
}