import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { CheckType } from '../dto/studies.enums';

@Entity({ name: 'studies' })
export class Study {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  type: CheckType;

  @Column('varchar')
  name: string;

  @Column('varchar')
  teacher: string;

  @Column('varchar')
  link?: string;

  @Column('varchar')
  connection?: string;

  @Column('varchar')
  userId: string;

  @ManyToOne(() => User, (user) => user.studies, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'userId' })
  user: User;

  @UpdateDateColumn()
  updatedAt: string;

  @CreateDateColumn()
  createdAt: string;
}
