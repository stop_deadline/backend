import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateStudyDto } from './dto/create-study.dto';
import { UpdateStudyDto } from './dto/update-study.dto';
import { Study } from './entities/study.entity';

@Injectable()
export class StudiesService {
  constructor(
    @InjectRepository(Study)
    private readonly studyRepository: Repository<Study>,
  ) {}

  findAll(): Promise<Study[]> {
    return this.studyRepository.find();
  }

  findByUserId(id: string): Promise<Study[]> {
    return this.studyRepository.findBy({ userId: id });
  }

  findOne(id: string): Promise<Study> {
    return this.studyRepository.findOneBy({ id });
  }

  create(user: User, createStudyDto: CreateStudyDto): Promise<Study> {
    const userId = user.id;
    return this.studyRepository.save({ userId, ...createStudyDto });
  }

  update(id: string, updateStudyDto: UpdateStudyDto): Promise<Study> {
    return this.studyRepository.save({ id, ...updateStudyDto });
  }

  async remove(id: string): Promise<boolean> {
    return !!(await this.studyRepository.delete(id));
  }
}
