import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Request,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { StudiesService } from './studies.service';
import { CreateStudyDto } from './dto/create-study.dto';
import { UpdateStudyDto } from './dto/update-study.dto';
import { UserObject } from 'src/common/types';
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { StudyModel } from './model/studies.model';

@ApiTags('Studies')
@Controller('studies')
export class StudiesController {
  constructor(private readonly studiesService: StudiesService) {}

  @ApiResponse({ type: [StudyModel] })
  @Get()
  findAll() {
    return this.studiesService.findAll();
  }

  @ApiResponse({ type: StudyModel })
  @Get('user/:id')
  findByUser(@Param('id') id: string) {
    return this.studiesService.findByUserId(id);
  }

  @ApiResponse({ type: StudyModel })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.studiesService.findOne(id);
  }

  @ApiResponse({ type: StudyModel })
  @UseGuards(AuthGuard('jwt'))
  @Post()
  create(
    @Request() { user }: UserObject,
    @Body() createStudyDto: CreateStudyDto,
  ) {
    return this.studiesService.create(user, createStudyDto);
  }

  @ApiResponse({ type: StudyModel })
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateStudyDto: UpdateStudyDto) {
    return this.studiesService.update(id, updateStudyDto);
  }

  @ApiResponse({ type: Boolean })
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.studiesService.remove(id);
  }
}
