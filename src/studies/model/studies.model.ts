import { ApiProperty } from '@nestjs/swagger';
import { CheckType } from '../dto/studies.enums';

export class StudyModel {
  @ApiProperty({
    example: 'd61db306-29e7-4caf-b9b8-4338526e71c5',
    description: 'id',
    type: String,
  })
  id: string;

  @ApiProperty({
    example: 'EXAM',
    description: 'type',
    enum: CheckType,
  })
  type: CheckType;

  @ApiProperty({
    example: 'Проектная деятельность',
    description: 'name',
    type: String,
  })
  name: string;

  @ApiProperty({
    example: 'преподаватель',
    description: 'teacher',
    type: String,
  })
  teacher: string;

  @ApiProperty({
    example: 'https://localhost:3000/...',
    description: 'link',
    type: String,
  })
  link?: string;

  @ApiProperty({
    example: 'example@gmail.com',
    description: 'link',
    type: String,
  })
  connection?: string;

  @ApiProperty({
    example: 'd61db306-29e7-4caf-b9b8-4338526e71c5',
    description: 'user id',
    type: String,
  })
  userId: string;

  @ApiProperty({
    example: '2022-06-07 14:58:17 +0000',
    description: 'updated at',
    type: String,
  })
  updatedAt: string;

  @ApiProperty({
    example: '2022-06-07 14:58:17 +0000',
    description: 'created at',
    type: String,
  })
  createdAt: string;
}
