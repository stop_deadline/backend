import { ApiProperty } from '@nestjs/swagger';

export class UserModel {
  @ApiProperty({
    example: 'd61db306-29e7-4caf-b9b8-4338526e71c5',
    description: 'id',
    type: String,
  })
  id: string;

  @ApiProperty({
    example: 'Ivan Ivanov',
    description: 'username',
    type: String,
  })
  username: string;

  @ApiProperty({ example: 'ivan123', description: 'login', type: String })
  login: string;

  @ApiProperty({
    example: '2022-06-07 14:58:17 +0000',
    description: 'updated at',
    type: String,
  })
  updatedAt: string;

  @ApiProperty({
    example: '2022-06-07 14:58:17 +0000',
    description: 'created at',
    type: String,
  })
  createdAt: string;
}
