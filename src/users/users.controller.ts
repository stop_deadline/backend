import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { ResponseDto, SignInDto, SignUpDto } from 'src/auth/dto/auth.dto';
import { AuthService } from 'src/auth/auth.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { UserModel } from './model/user.model';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {}

  @ApiResponse({type: ResponseDto})
  @Post('sign-up')
  async signUp(@Body() body: SignUpDto): Promise<ResponseDto> {
    return this.authService.signUp(body);
  }

  @ApiResponse({type: ResponseDto})
  @Post('sign-in')
  async signIn(@Body() body: SignInDto): Promise<ResponseDto> {
    return this.authService.signIn(body);
  }

  @ApiResponse({type: [UserModel]})
  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @ApiResponse({type: UserModel})
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(id);
  }

  @ApiResponse({type: Boolean})
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(id);
  }
}
