
import { Diary } from 'src/diary/entities/diary.entity';
import { Link } from 'src/links/entities/link.entity';
import { Study } from 'src/studies/entities/study.entity';
import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  username: string;

  @Column('varchar', { unique: true })
  login: string;

  @Column('varchar')
  password: string;

  @OneToMany(() => Diary, (diary) => diary.user, { cascade: true })
  diaries?: Diary[];

  @OneToMany(() => Link, (link) => link.user, { cascade: true })
  links?: Link[];

  @OneToMany(() => Study, (study) => study.user, { cascade: true })
  studies?: Study[];

  @UpdateDateColumn()
  updatedAt: string;

  @CreateDateColumn()
  createdAt: string;
}
