import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { StudiesModule } from './studies/studies.module';
import { DiaryModule } from './diary/diary.module';
import { LinksModule } from './links/links.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DB_URL } from './config';
import { User } from './users/entities/user.entity';
import { Study } from './studies/entities/study.entity';
import { Diary } from './diary/entities/diary.entity';
import { Link } from './links/entities/link.entity';

@Module({
  imports: [
    UsersModule,
    StudiesModule,
    DiaryModule,
    LinksModule,
    AuthModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: DB_URL,
      logging: true,
      entities: [User, Study, Diary, Link],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
