import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateLinkDto } from './dto/create-link.dto';
import { UpdateLinkDto } from './dto/update-link.dto';
import { Link } from './entities/link.entity';

@Injectable()
export class LinksService {
  constructor(
    @InjectRepository(Link)
    private readonly linkRepository: Repository<Link>,
  ) {}

  findAll(): Promise<Link[]> {
    return this.linkRepository.find();
  }

  findByUserId(id: string): Promise<Link[]> {
    return this.linkRepository.findBy({ userId: id });
  }

  findOne(id: string): Promise<Link> {
    return this.linkRepository.findOneBy({ id });
  }

  create(user: User, createLinkDto: CreateLinkDto): Promise<Link> {
    const userId = user.id;
    return this.linkRepository.save({ userId, ...createLinkDto });
  }

  update(id: string, updateLinkDto: UpdateLinkDto): Promise<Link> {
    return this.linkRepository.save({ id, ...updateLinkDto });
  }

  async remove(id: string): Promise<boolean> {
    return !!(await this.linkRepository.delete(id));
  }
}
