import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Request,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { LinksService } from './links.service';
import { CreateLinkDto } from './dto/create-link.dto';
import { UpdateLinkDto } from './dto/update-link.dto';
import { AuthGuard } from '@nestjs/passport';
import { UserObject } from 'src/common/types';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { LinkModel } from './model/links.model';

@ApiTags('Links')
@Controller('links')
export class LinksController {
  constructor(private readonly linksService: LinksService) {}

  @ApiResponse({ type: [LinkModel] })
  @Get()
  findAll() {
    return this.linksService.findAll();
  }

  @ApiResponse({ type: LinkModel })
  @Get('user/:id')
  findByUser(@Param('id') id: string) {
    return this.linksService.findByUserId(id);
  }

  @ApiResponse({ type: LinkModel })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.linksService.findOne(id);
  }

  @ApiResponse({ type: LinkModel })
  @UseGuards(AuthGuard('jwt'))
  @Post()
  create(
    @Request() { user }: UserObject,
    @Body() createLinkDto: CreateLinkDto,
  ) {
    return this.linksService.create(user, createLinkDto);
  }

  @ApiResponse({ type: LinkModel })
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLinkDto: UpdateLinkDto) {
    return this.linksService.update(id, updateLinkDto);
  }

  @ApiResponse({ type: Boolean })
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.linksService.remove(id);
  }
}
