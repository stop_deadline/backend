import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { LinkType } from './link.enums';

export class CreateLinkDto {
  @ApiProperty({
    example: 'OTHER',
    description: 'type',
    enum: LinkType,
  })
  @IsNotEmpty()
  @IsEnum(LinkType)
  type: LinkType;

  @ApiProperty({
    example: 'Правила оформления отчетов',
    description: 'description',
    type: String,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  description?: string;

  @ApiProperty({
    example: 'https://localhost:3000/...',
    description: 'description',
    type: String,
    nullable: true,
  })
  @IsNotEmpty()
  @IsString()
  link: string;
}
