import { User } from 'src/users/entities/user.entity';

export type UserObject = {
  user: User;
};
