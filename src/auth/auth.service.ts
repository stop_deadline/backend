import { ForbiddenException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { SignInDto, SignUpDto, ResponseDto } from './dto/auth.dto';
import { hash, compare } from 'bcrypt';
import { User } from 'src/users/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

  async signUp(dto: SignUpDto): Promise<ResponseDto> {
    const isExistUser = !!await this.usersRepository.count({where: {login: dto.login}});
    if (isExistUser) {
      throw new ForbiddenException(
        'Пользователь с таким логином уже существует',
      );
    }

    dto.password = await hash(dto.password, 13);
    const user = await this.usersRepository.save(dto);
    
    const token = await this.jwtService.signAsync({ sub: user.id });
    return { token, user };
  }

  async signIn(dto: SignInDto): Promise<ResponseDto> {
    const user = await this.usersRepository.findOneBy({ login: dto.login });
    if (!user) {
      throw new ForbiddenException(
        'Пользователя с таким логином не существует',
      );
    }

    const isCorrectPassword = await compare(dto.password, user.password);
    if (!isCorrectPassword) {
      throw new ForbiddenException('Невалидный логин или пароль');
    }

    const token = await this.jwtService.signAsync({ sub: user.id });
    return { token, user };
  }
}
