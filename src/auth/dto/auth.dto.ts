import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';
import { User } from 'src/users/entities/user.entity';
import { UserModel } from 'src/users/model/user.model';

export class SignUpDto {
  @ApiProperty({
    example: 'Ivan Ivanov',
    description: 'username',
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  username: string;

  @ApiProperty({ example: 'ivan123', description: 'login', type: String })
  @IsNotEmpty()
  @IsString()
  login: string;

  @ApiProperty({ example: 'ivan123456', description: 'password', type: String })
  @IsNotEmpty()
  @IsString()
  @Length(6, 32)
  password: string;
}

export class ResponseDto {
  @ApiProperty({
    example: 'ed48ba07-60f5-4c0f-9c29-81ebdd4173bb',
    description: 'token',
    type: String,
  })
  token: string;

  @ApiProperty({type: UserModel})
  user: UserModel;
}

export class SignInDto {
  @ApiProperty({ example: 'ivan123', description: 'login', type: String })
  @IsNotEmpty()
  @IsString()
  login: string;

  @ApiProperty({ example: 'ivan123456', description: 'password', type: String })
  @IsNotEmpty()
  @IsString()
  password: string;
}
