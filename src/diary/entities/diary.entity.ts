import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'diaries' })
export class Diary {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  subject: string;

  @Column('varchar', { nullable: true })
  subgroup?: string;

  @Column('varchar')
  date: string;

  @Column('varchar')
  task: string;

  @Column('varchar')
  teacher: string;

  @Column('varchar')
  userId: string;

  @ManyToOne(() => User, (user) => user.diaries, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'userId' })
  user: User;

  @UpdateDateColumn()
  updatedAt: string;

  @CreateDateColumn()
  createdAt: string;
}
