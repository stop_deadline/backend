import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateDiaryDto } from './dto/create-diary.dto';
import { UpdateDiaryDto } from './dto/update-diary.dto';
import { Diary } from './entities/diary.entity';

@Injectable()
export class DiaryService {
  constructor(
    @InjectRepository(Diary)
    private readonly diaryRepository: Repository<Diary>,
  ) {}

  findAll(): Promise<Diary[]> {
    return this.diaryRepository.find();
  }

  findOne(id: string): Promise<Diary> {
    return this.diaryRepository.findOneBy({ id });
  }

  findByUserId(id: string): Promise<Diary[]> {
    return this.diaryRepository.findBy({userId: id});
  }

  create(user: User, createDiaryDto: CreateDiaryDto): Promise<Diary> {
    const userId = user.id;
    return this.diaryRepository.save({ userId, ...createDiaryDto });
  }

  update(id: string, updateDiaryDto: UpdateDiaryDto): Promise<Diary> {
    return this.diaryRepository.save({ id, ...updateDiaryDto });
  }

  async remove(id: string): Promise<boolean> {
    return !!(await this.diaryRepository.delete(id));
  }
}
