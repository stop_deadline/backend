import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateDiaryDto {
  @ApiProperty({
    example: 'Web-разработка',
    description: 'subject',
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  subject: string;

  @ApiProperty({
    example: '1',
    description: 'subgroup',
    type: String,
    nullable: true
  })
  @IsOptional()
  @IsString()
  subgroup?: string;

  @ApiProperty({
    example: '1654793428',
    description: 'date',
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  date: string;

  @ApiProperty({
    example: 'курсовой проект',
    description: 'task',
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  task: string;

  @ApiProperty({
    example: 'best препод',
    description: 'teacher',
    type: String,
  })
  @IsNotEmpty()
  @IsString()
  teacher: string;
}
