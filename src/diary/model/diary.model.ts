import { ApiProperty } from '@nestjs/swagger';

export class DiaryModel {
  @ApiProperty({
    example: 'd61db306-29e7-4caf-b9b8-4338526e71c5',
    description: 'id',
    type: String,
  })
  id: string;

  @ApiProperty({
    example: 'Web-разработка',
    description: 'subject',
    type: String,
  })
  subject: string;

  @ApiProperty({
    example: '1',
    description: 'subgroup',
    type: String,
    nullable: true
  })
  subgroup?: string;

  @ApiProperty({
    example: '1654793428',
    description: 'date',
    type: String,
  })
  date: string;

  @ApiProperty({
    example: 'курсовой проект',
    description: 'task',
    type: String,
  })
  task: string;

  @ApiProperty({
    example: 'best препод',
    description: 'teacher',
    type: String,
  })
  teacher: string;

  @ApiProperty({
    example: 'd61db306-29e7-4caf-b9b8-4338526e71c5',
    description: 'user id',
    type: String,
  })
  userId: string;

  @ApiProperty({
    example: '2022-06-07 14:58:17 +0000',
    description: 'updated at',
    type: String,
  })
  updatedAt: string;

  @ApiProperty({
    example: '2022-06-07 14:58:17 +0000',
    description: 'created at',
    type: String,
  })
  createdAt: string;
}
