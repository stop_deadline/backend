import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Request,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { UserObject } from 'src/common/types';
import { DiaryService } from './diary.service';
import { CreateDiaryDto } from './dto/create-diary.dto';
import { UpdateDiaryDto } from './dto/update-diary.dto';
import { DiaryModel } from './model/diary.model';

@ApiTags('Diary')
@Controller('diary')
export class DiaryController {
  constructor(private readonly diaryService: DiaryService) {}

  @ApiResponse({ type: [DiaryModel] })
  @Get()
  findAll() {
    return this.diaryService.findAll();
  }

  @ApiResponse({ type: DiaryModel })
  @Get('user/:id')
  findByUser(@Param('id') id: string) {
    return this.diaryService.findByUserId(id);
  }

  @ApiResponse({ type: DiaryModel })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.diaryService.findOne(id);
  }

  @ApiResponse({ type: DiaryModel })
  @UseGuards(AuthGuard('jwt'))
  @Post()
  create(
    @Request() { user }: UserObject,
    @Body() createDiaryDto: CreateDiaryDto,
  ) {
    return this.diaryService.create(user, createDiaryDto);
  }

  @ApiResponse({ type: DiaryModel })
  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDiaryDto: UpdateDiaryDto) {
    return this.diaryService.update(id, updateDiaryDto);
  }

  @ApiResponse({ type: Boolean })
  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.diaryService.remove(id);
  }
}
